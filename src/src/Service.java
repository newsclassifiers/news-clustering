package src;
import static java.lang.Double.*;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilderFactory;

class ValueComparator implements Comparator<String> {

    Map<String, Double> base;
    public ValueComparator(Map<String, Double> base) {
        this.base = base;
    }

    // Note: this comparator imposes orderings that are inconsistent with equals.    
    public int compare(String a, String b) {
        if (base.get(a) >= base.get(b)) {
            return -1;
        } else {
            return 1;
        } // returning 0 would merge keys
    }
}

public class Service {	

	private static final String pathR= "C:/Users/Fa/Desktop/496/alchemydocs\\";
	private static final String pathRNews= "C:/Users/Fa/Desktop/496/folderToIndex\\";
	private static final String pathRNewsInfo = "C:/Users/Fa/Desktop/496/";
	private static final String pathRNewsTFIDF = "C:/Users/Fa/Desktop/496/";
	private static final String pathWTR = "C:/Users/Fa/workspace/bitirme/src/src/stopwords.txt";
	private static ArrayList<ArrayList<String>> gruptopic=new ArrayList<ArrayList<String>>();
	private static ArrayList<ArrayList<String>> grupcluster=new ArrayList<ArrayList<String>>();
//Get Maps			
	private static void removeUnnecessaryWords(HashMap<String,Integer> map) throws FileNotFoundException{		
		Scanner s = new Scanner(new File(pathWTR));
		
		ArrayList<String> wordsToRemove = new ArrayList<String>();
		while (s.hasNext()){
		    wordsToRemove.add(s.next());
		}
		s.close();
		
		for(int i=0; i<wordsToRemove.size(); i++)
			map.remove(wordsToRemove.get(i));		
	}
	public static void ReadText(String path) throws IOException{
		File file = null;
	    FileReader fr = null;
	    LineNumberReader lnr = null;
	    ArrayList<String> myList = new ArrayList<String>();
	    ArrayList<String>myList2=new ArrayList<String>();
	    ArrayList<String>myList3=new ArrayList<String>();
	    HashMap<Integer,String>map=new HashMap<Integer,String>();
	    try {
	        file = new File(path);
	        fr = new FileReader(file);           
	        lnr = new LineNumberReader(fr);
	        String line = "";           
	        boolean denetleme=false;
	        while ((line = lnr.readLine()) != null&& denetleme!=true) {
	        		boolean varmi=false;
	        		for(int i=0;i<line.length();i++){
	        			for(int j=i+1;j<=line.length();j++){
	        				if(line.substring(i,j).equalsIgnoreCase("TOPICS")){
	        					varmi=true;
	        				
	        				}
	        				}
	        			
	        		}
	        		if(varmi==true){
	        			String docName=null;
	        			String topicname=null;
	        			int son=0;
	        			docName=line.substring(0,line.indexOf('T')-1);//bu k�sma bak
	        		    topicname=line.substring(line.indexOf('S')+2,line.length());
	        		   
	        		    myList.add(docName);
        		        myList2.add(topicname);
	        		
        		        if(docName.equalsIgnoreCase("1500")){
        		        	denetleme=true;
        		        }
	        		  
					

	        		}
	        		
	        		

	        }

	        for(int i=0;i<myList.size();i++){
	        	if(myList.get(i).length()==1){
	        		String docname="000"+myList.get(i)+".txt";
	        		myList3.add(docname);
	        	}
	        	else if(myList.get(i).length()==2){
	        		String docname="00"+myList.get(i)+".txt";
	        		myList3.add(docname);
	        	}
	        	else if(myList.get(i).length()==3){
	        		String docname="0"+myList.get(i)+".txt";
	        		myList3.add(docname);
	        	}
	        	else{
	        		String docname=myList.get(i)+".txt";
	        		myList3.add(docname);
	        	}
	        	
	        }
   Inserttopic(myList3,myList2);

	    } finally {
	        if (fr != null) {
	            fr.close();
	        }
	        if (lnr != null) {
	            lnr.close();
	        }
	    }
	    
	}
	public static void topiccluster() throws FileNotFoundException, UnsupportedEncodingException{
	ResultSet sonuc=Service.Topic();
	
		
		ArrayList<String>topiclist=new ArrayList<String>();
		int i=1;
		 try {
			
	            while (sonuc.next()) {
	            	
	            	   
	                topiclist.add(sonuc.getString(i));
	             
	                
	            }
	        } catch (Exception ex) {
	            System.out.println(ex.getMessage());
	        }
		 ArrayList<ArrayList<String>> grup = new ArrayList<ArrayList<String>>();
		 for(int a=0;a<topiclist.size();a++){
			ResultSet sorgu=Service.getGrup(topiclist.get(a));
//			System.out.println(topiclist.get(a));
			ArrayList<String>grup1=new ArrayList<String>();
			try {
				
	            while (sorgu.next()) {
	            	
	            	if(!grup1.contains(sorgu.getString(1))&&!grup.contains(sorgu.getString(1))){   
	                grup1.add(sorgu.getString(1));
	            	}
	                
	            }
	        } catch (Exception ex) {
	            System.out.println(ex.getMessage());
	        }
			
			grup.add(grup1);
			
			
		 }
//		 HashMap<String,Integer> kontrol=new HashMap<String,Integer>();
//		 String []array=Service.getDocumentNames();
//		 System.out.println(array[0]);
//		
//			 ResultSet sonuc2=Service.getSayi(kontrol,array);
//			System.out.println(kontrol);
//			LinkedHashMap<String, Double> comparisonMap = Service.getComparisonMap(mainMap);		
//	        TreeMap<String,Double> sortedMap = Service.sortMap(comparisonMap); 	 
//	        Set<String> both = new HashSet<String>(kontrol.keySet());
//	       
//	
//		
//	 for(int k=0;k<grup.size();k++){
//			 if(grup.get(k).size()>2){
//				 
//			 
//		 System.out.println(grup.get(k));
//			 }
//		 }
		 ArrayList<ArrayList<String>> grup1 = new ArrayList<ArrayList<String>>();
//		 newsgrup(grup);
		 Writetopiccluster(grup);
		 newsgrup(grupcluster);
	}
	@SuppressWarnings("resource")
	public static void fmeasure() throws FileNotFoundException, UnsupportedEncodingException{
		PrintWriter writer = new PrintWriter(pathRNewsInfo+"fmeasure1.txt", "UTF-8");
		for(int i=0;i<grupcluster.size();i++){
			for(int k = 0;k<gruptopic.size();k++){
			
			
				
				double deger=fmesaurelist(grupcluster.get(i),gruptopic.get(k));
				if(deger>0&&deger<1&&deger!=2){
					if(!isNaN(deger)){
					writer.println("GrupCluster"+i+"---"+"GrupTopic"+k+"------>"+deger);
					}
					}
			}
		}
		writer.close();
	}
	public static double fmesaurelist(ArrayList<String>array1,ArrayList<String>array2){
		double sayi=0;
		double f1=0;
		for (int i=0;i<array1.size();i++){
			for(int k=0;k<array2.size();k++){
				if(array1.get(i).equals(array2.get(k))){
					sayi++;
				}
			}
			
		}
	
	
		if(array1.size()>0&&array2.size()>0){
		double array1length=array1.size();	
		double recall=sayi/array1length;
		double array2length=array2.size();
		double precision=sayi/array2length;
		 f1=2*(recall*precision)/(recall+precision);
		 System.out.println(f1);
		}
	
		
	return f1;
	
		}
	private static HashMap<String, Integer> getTFMap(String documentName){
		
		HashMap<String,Integer> tfMap = new HashMap<String,Integer>();
		
		try {			
			FileInputStream in = new FileInputStream(pathRNews+documentName);
			FileChannel filech = in.getChannel();		
			Integer one = new Integer(1);
			int fileLen = (int) filech.size();
			MappedByteBuffer buf = filech.map(FileChannel.MapMode.READ_ONLY, 0, fileLen);
			Charset chars = Charset.forName("ISO-8859-1");
			CharsetDecoder dec = chars.newDecoder();
			CharBuffer charBuf = dec.decode(buf);
			Pattern linePatt = Pattern.compile(".*$", Pattern.MULTILINE);
			Pattern wordBrkPatt = Pattern.compile("[\\p{Punct}\\s}]");
			Matcher lineM = linePatt.matcher(charBuf);
			
			while (lineM.find()) {
				CharSequence lineSeq = lineM.group();
				String words[] = wordBrkPatt.split(lineSeq);
				for (int i = 0, n = words.length; i < n; i++) {
					if (words[i].length() > 2) {
						Integer frequency = (Integer) tfMap.get(words[i].toLowerCase());
						if (frequency == null) {
							frequency = one;
						} 
						else {
							int value = frequency.intValue();
							frequency = new Integer(value + 1);
						}
						try {
							Integer.parseInt(words[i]);
						} 
						catch (Exception e) {
							tfMap.put(words[i].toLowerCase(), frequency);
						}				
					}
				}
			}
			removeUnnecessaryWords(tfMap);
		} 
		catch (Exception e) {
			tfMap.clear();
			tfMap = null;
		}		
		return tfMap;
	}
	public static void writeFMeasure(LinkedHashMap<String,HashMap<String,Integer>>mainMap)throws FileNotFoundException, UnsupportedEncodingException{
		
		HashMap<String,Double> result=getMeasureMap(mainMap);
		PrintWriter writer = new PrintWriter(pathRNewsInfo+"-f1-score"+"-tfidf.txt", "UTF-8");
		
		for(Map.Entry<String, Double> entry : result.entrySet()){
			String ad=entry.getKey();
			Double fscore=entry.getValue();
			if(fscore!=0||fscore!=null){
			writer.println(ad+"    "+fscore);
			}
		}
		System.out.println("f-measure.txt is ready ");
	}
private static HashMap<String, Double> getMeasureMap(LinkedHashMap<String, HashMap<String, Integer>> mainMap){
		
		HashMap<String, Double> comparisonMap = new HashMap<String, Double>();
		
		for (Map.Entry<String, HashMap<String, Integer>> entry1 : mainMap.entrySet()){ 
			
			String doc1Name = entry1.getKey(); 
			
			HashMap<String, Integer> map1 = entry1.getValue();
			
			for (Map.Entry<String, HashMap<String, Integer>> entry2 : mainMap.entrySet()){
				
				String doc2Name = entry2.getKey();
				
				HashMap<String, Integer> map2 = entry2.getValue();				
				
				if(!doc1Name.equals(doc2Name) &&
					!(comparisonMap.containsKey(doc1Name+"<->"+doc2Name) 
							|| comparisonMap.containsKey(doc2Name+"<->"+doc1Name))){
					
					double fscore =computeFMeasure(map1, map2);				
					comparisonMap.put(doc1Name+"<->"+doc2Name, fscore);					
				}				
			}
		}		
		return comparisonMap;
	}
private static ArrayList<HashMap<String, Integer>> getMaps(String[] documentNames) throws FileNotFoundException{
	ArrayList<HashMap<String, Integer>> maps = new ArrayList<HashMap<String,Integer>>();
	
	for (int i = 0; i < documentNames.length; i++) {
		maps.add(getMap(documentNames[i]));
	}		
	return maps;
}
private static HashMap<String, Integer> getMap(String documentName) throws FileNotFoundException{
	
	HashMap<String,Integer> map = new HashMap<String,Integer>();
	
	try {			
		FileInputStream in = new FileInputStream(pathRNews+documentName);
		FileChannel filech = in.getChannel();		
		Integer one = new Integer(1);
		int fileLen = (int) filech.size();
		MappedByteBuffer buf = filech.map(FileChannel.MapMode.READ_ONLY, 0, fileLen);
		Charset chars = Charset.forName("ISO-8859-1");
		CharsetDecoder dec = chars.newDecoder();
		CharBuffer charBuf = dec.decode(buf);
		Pattern linePatt = Pattern.compile(".*$", Pattern.MULTILINE);
		Pattern wordBrkPatt = Pattern.compile("[\\p{Punct}\\s}]");
		Matcher lineM = linePatt.matcher(charBuf);
		
		while (lineM.find()) {
			CharSequence lineSeq = lineM.group();
			String words[] = wordBrkPatt.split(lineSeq);
			for (int i = 0, n = words.length; i < n; i++) {
				if (words[i].length() > 2) {
					Integer frequency = (Integer) map.get(words[i].toLowerCase());
					if (frequency == null) {
						frequency = one;
					} 
					else {
						int value = frequency.intValue();
						frequency = new Integer(value + 1);
					}
					try {
						Integer.parseInt(words[i]);
					} 
					catch (Exception e) {
						map.put(words[i].toLowerCase(), frequency);
					}				
				}
			}
		}
	} 
	catch (Exception e) {
		map.clear();
		map = null;
	}		
	removeUnnecessaryWords(map);
	return map;
}

//public static LinkedHashMap<String, HashMap<String, Integer>> getAll2() throws FileNotFoundException{
//	
//	String[] documentNames = getDocumentNames();
//	
//	ArrayList<HashMap<String, Integer>> maps = getMaps(documentNames);
//	
//	if(maps != null || maps.size() != 0){
//		
//		System.out.println("\nInserting documents...");			
//		
//		
//		LinkedHashMap<String, HashMap<String, Integer>> mainMap3 = new LinkedHashMap<String, HashMap<String, Integer>>();			
//
//		String currentDocumentName;
//
//		HashMap<String, Integer> tfidfMap;
//					
//		int pointer = 0;
//		
//		for (HashMap<String, Integer> currentMap : maps) {						
//			
//			tfidfMap = new HashMap<String, Integer>();				
//			
//			currentDocumentName = documentNames[pointer];
//			
//			pointer++;
//			
//			for (String word : currentMap.keySet()) {
//				
//				int tf = computeTF(currentMap, word);
//			
//																				
//				
//				tfidfMap.put(word, tf);
//			}														
//			mainMap3.put(currentDocumentName, tfidfMap);					
//		} 
//		
//		System.out.println("Documents does not exist anymore!");
//		return mainMap3;				
//	}
//	
//	return null;}
public static double computeFMeasure(HashMap<String,Integer>tfMap1,HashMap<String,Integer>tfMap2)throws ArithmeticException{
    Set<String> both = new HashSet<String>(tfMap1.keySet());
       both.retainAll(tfMap2.keySet());
         double sclar=1;
         double tf1=1;
         double tf2=1;
         for(String k:both) {
       
       	    
       	tf1+=tfMap1.get(k);
       	
       	tf2+=tfMap2.get(k);
       	if(tfMap1.get(k)<tfMap2.get(k)){
       		sclar+=tfMap1.get(k);
       	}
       	else{
       		sclar+=tfMap2.get(k);
       	}
       	
       }   
       
         sclar--;
         tf1--;
         tf2--;
         
     double precision=sclar/tf1;
     
 double recall=sclar/tf2;

 double fmeasure=2*(precision*recall)/(precision+recall);
      
	return fmeasure;
}
	private static ArrayList<HashMap<String, Integer>> getTFMaps(String[] documentNames){		
		HashMap<String, Integer> TFMap;
		ArrayList<HashMap<String, Integer>> TFMaps = new ArrayList<HashMap<String,Integer>>();
		
		for (int i = 0; i < documentNames.length; i++) {
			TFMap = getTFMap(documentNames[i]);
			
			if(TFMap != null)
				TFMaps.add(TFMap);			
			else
				return null;
		}
		
		return TFMaps;
	}

	public static String[] getDocumentNames(){
		File folder = new File(pathRNews);
		String[] documentNames = folder.list();
		return documentNames;
	}
	
//Compute TF-IDF	
	private static int computeTF(HashMap<String,Integer> map, String word){						
		return map.get(word);	
	}	
	public static String[] getXMLNames(){
		File folder = new File(pathR);
		String[] documentNames = folder.list();
		return documentNames;
	}
	
	private static double computeIDF(ArrayList<HashMap<String, Integer>> maps, String word){
		int totalFileNumberWordExists = 0;					
		for (HashMap<String, Integer> map : maps) {
			if(map.get(word) != null)
				totalFileNumberWordExists++;												
		}						
		int totalFileNumber = maps.size();
		
		return Math.log((double)totalFileNumber/totalFileNumberWordExists);		
	}

//GET ALL			
public static LinkedHashMap<String, HashMap<String, Double>> getAll() throws SQLException{
		ArrayList<HashMap<String, Integer>> tfMaps;
		
		String[] documentNames = getDocumentNames();
		
		if(documentNames != null && documentNames.length != 0)
			tfMaps = getTFMaps(documentNames);
		else
			tfMaps = null;
		
		if(tfMaps != null && tfMaps.size() != 0){
			System.out.println("\nTFIDF map is being created...");			
			
			long start = System.currentTimeMillis();			
			String currentDocumentName;
			HashMap<String, Double> tfidfMap;
			LinkedHashMap<String, HashMap<String, Double>> mainMap = new LinkedHashMap<String, HashMap<String, Double>>();			
						
			int pointer = 0;
			
			for (HashMap<String, Integer> currentTFMap : tfMaps) {						
				
				tfidfMap = new HashMap<String, Double>();								
				currentDocumentName = documentNames[pointer];
//				Veritabani.baglan();	
				for (String word : currentTFMap.keySet()) {
					
					
					int tf = computeTF(currentTFMap, word);
					double idf = computeIDF(tfMaps,word);
					double tf_idf = tf * idf;																
//					try{
//						
//					PreparedStatement sorgu=Veritabani.getBaglanti().prepareStatement("Insert into tfidf values('"+currentDocumentName+"','"+word+"','"+tf+"','"+idf+"','"+tf_idf+"');");
//					sorgu.executeUpdate();
//				}
//			
//			  catch (Exception e) {
//		          System.out.println(e.getMessage());
//		      }
					tfidfMap.put(word, tf_idf);		
					}														
				mainMap.put(currentDocumentName, tfidfMap);					
				pointer++;
				System.out.println(currentDocumentName);
			} 
			long end = System.currentTimeMillis();
			double elapsedTime = (double)(end-start)/1000;
			System.out.println(mainMap.size()+" documents inserted to TFIDF-Map in "+elapsedTime+" seconds.");							
			System.out.println("Inserted documents: "+mainMap.keySet());
			
			return mainMap;				
		}
		else{
			System.out.println("TFIDF Map could not be loaded!");
			return null;
		}
	}
	
//Helper Methods		
	public static TreeMap<String,Double> sortMap(HashMap<String, Double> tfidfMap){		
        ValueComparator bvc =  new ValueComparator(tfidfMap);
        TreeMap<String,Double> sorted_map = new TreeMap<String,Double>(bvc);
        sorted_map.putAll(tfidfMap);        
        return sorted_map;
	}
	
	private static String getCommonWords(HashMap<String, Double> tfidfMap1, HashMap<String, Double> tfidfMap2) {		
		Set<String> both = new HashSet<String>(tfidfMap1.keySet());
		both.retainAll(tfidfMap2.keySet());					
		return (both.toString());		
	}
	public static double cosineSimilarity2(LinkedHashMap<String, HashMap<String, Double>> mainMap,String doc1,String doc2){
		double sonuc=0;
		HashMap<String,Double>tfidfMap1=mainMap.get(doc1);
		HashMap<String,Double>tfidfMap2=mainMap.get(doc2);
		   Set<String> both = new HashSet<String>(tfidfMap1.keySet());
	         both.retainAll(tfidfMap2.keySet());
	         if(both.size()>4){
		
		sonuc=cosineSimilarity(tfidfMap1,tfidfMap2);
		return sonuc;
		}
	         else{
	        	 return 0;
	         }
	}
	private static double cosineSimilarity(HashMap<String, Double> tfidfMap1, HashMap<String, Double> tfidfMap2) {		
         Set<String> both = new HashSet<String>(tfidfMap1.keySet());
         both.retainAll(tfidfMap2.keySet());
         
         double sclar = 0, norm1 = 0, norm2 = 0;         
         
         for (String k : both) sclar += tfidfMap1.get(k) * tfidfMap2.get(k);
         for (String k : tfidfMap1.keySet()) norm1 += tfidfMap1.get(k) * tfidfMap1.get(k);
         for (String k : tfidfMap2.keySet()) norm2 += tfidfMap2.get(k) * tfidfMap2.get(k);
        
         double result = sclar / Math.sqrt(norm1 * norm2);
         
		 return result;	
	}
	
	public static LinkedHashMap<String, Double> getComparisonMap(LinkedHashMap<String, HashMap<String, Double>> mainMap){
		
		LinkedHashMap<String, Double> comparisonMap = new LinkedHashMap<String, Double>();
		
		for (Map.Entry<String, HashMap<String, Double>> entry1 : mainMap.entrySet()){ 
			
			String doc1Name = entry1.getKey(); 			
			HashMap<String, Double> map1 = entry1.getValue();
			
			for (Map.Entry<String, HashMap<String, Double>> entry2 : mainMap.entrySet()){
				
				String doc2Name = entry2.getKey();				
				HashMap<String, Double> map2 = entry2.getValue();				
				
				if(!doc1Name.equals(doc2Name) &&
					!(comparisonMap.containsKey(doc1Name+"<>"+doc2Name) 
							|| comparisonMap.containsKey(doc2Name+"<>"+doc1Name))){
					   Set<String> both = new HashSet<String>(map1.keySet());
				         both.retainAll(map2.keySet());
//					if(both.size()>9){
				         double similarity = cosineSimilarity(map1, map2);	
				         if(similarity<0.95&&similarity>0.1){
				     
					comparisonMap.put(doc1Name+"<>"+doc2Name, similarity);
//				         }
					}
				}				
			}
		}		
		return comparisonMap;
	}
	
//Clustering
private static HashMap<String,Integer> getCluster2(LinkedHashMap<String, HashMap<String, Double>> mainMap){		
		
		LinkedHashMap<String, Double> comparisonMap = getComparisonMap(mainMap);		
		TreeMap<String,Double> sortedComparisonMap = sortMap(comparisonMap);        
        
		LinkedHashSet<String> sortedComparisonSet = new LinkedHashSet<String>();		
		sortedComparisonSet.addAll(sortedComparisonMap.keySet());
		HashMap<String,Integer>clusters=new HashMap<String,Integer>();
		LinkedHashSet<String>alinan=new LinkedHashSet<String>();
		 
		int grup_Id=0;
		int uzunluk=sortedComparisonSet.size();
		int uzun=0;
		System.out.println(uzunluk);
		while(uzun<uzunluk-1){
			
			boolean atla=true;
		grup_Id++;
		for(String str : sortedComparisonSet){		
			uzun++;
			String doc1 = str.substring(0,str.indexOf('<'));
			String doc2 = str.substring(str.indexOf('>')+1);
			double sonuc=cosineSimilarity2(mainMap,doc1,doc2);
			if(sonuc>0.25){
			if(alinan.size()==0){
				clusters.put(doc1,grup_Id);
				clusters.put(doc2,grup_Id);
				
				alinan.add(doc1);
				alinan.add(doc2);
				ResultSet sorgu=getDoc1(doc1);
				ResultSet sorgu1=getDoc1(doc2);
				try {
					
		            while (sorgu.next()) {
		            	
		            	if(!alinan.contains(sorgu.getString(1))){   
		            		clusters.put(sorgu.getString(1), grup_Id);
		            		System.out.println(sorgu1.getString(1));
		            	}
		                
		            }
		        } catch (Exception ex) {
		            System.out.println(ex.getMessage());
		        }
				try {
					
		            while (sorgu1.next()) {
		            	
		            	if(!alinan.contains(sorgu.getString(1))){   
		                clusters.put(sorgu1.getString(1),grup_Id);
		                System.out.println(sorgu1.getString(1));
		            	}
		                
		            }
		        } catch (Exception ex) {
		            System.out.println(ex.getMessage());
		        }
			}
					
			
			else if(alinan.contains(doc1)&&!alinan.contains(doc2)){
				uzun++;
				int grup=clusters.get(doc1);
				clusters.put(doc2, grup);
				
				ResultSet sorgu=getDoc1(doc2);
				try {
					
		            while (sorgu.next()) {
		            	
		            	if(!alinan.contains(sorgu.getString(1))){   
		            		clusters.put(sorgu.getString(1), grup);
		            		alinan.add(sorgu.getString(1));
		            		System.out.println(sorgu.getString(1));
		            	}
		                
		            }
		        } catch (Exception ex) {
		            System.out.println(ex.getMessage());
		        }
				alinan.add(doc2);
				atla=false;
				
			}

			 else if(alinan.contains(doc2)&&!alinan.contains(doc1)){
			uzun++;
				 System.out.println("gelesesme�lkrw�kre");
				 alinan.add(doc1);
				int grup=clusters.get(doc2);
				clusters.put(doc1, grup);
				ResultSet sorgu=getDoc1(doc1);
				try {
					
		            while (sorgu.next()) {
		            	
		            	if(!alinan.contains(sorgu.getString(1))){   
		            		clusters.put(sorgu.getString(1), grup);
		            		alinan.add(sorgu.getString(1));
		            	}
		                
		            }
		        } catch (Exception ex) {
		            System.out.println(ex.getMessage());
		        }
				
				
				
				atla=false;
			}
			 else if(!alinan.contains(doc1)&&!alinan.contains(doc2)&&atla==true){
				 uzun++;
				 grup_Id++;
				 clusters.put(doc1, grup_Id);
				 clusters.put(doc2, grup_Id);
				 alinan.add(doc1);
				 alinan.add(doc2);
				 ResultSet sorgu=getDoc1(doc1);
					ResultSet sorgu1=getDoc1(doc2);
					try {
						
			            while (sorgu.next()) {
			            	
			            	if(!alinan.contains(sorgu.getString(1))){   
			            		clusters.put(sorgu.getString(1), grup_Id);
			            		alinan.add(sorgu.getString(1));
			            	}
			                
			            }
			        } catch (Exception ex) {
			            System.out.println(ex.getMessage());
			        }
					try {
						
			            while (sorgu1.next()) {
			            	
			            	if(!alinan.contains(sorgu.getString(1))){   
			                clusters.put(sorgu1.getString(1),grup_Id);
			                alinan.add(sorgu1.getString(1));
			            	}
			                
			            }
			        } catch (Exception ex) {
			            System.out.println(ex.getMessage());
			        }
				 
			 }
			 else if(alinan.contains(doc1)&&alinan.contains(doc2)){
				 uzun++;
				continue; 
			 }
			}
		}
		
		}
			
		
		return clusters;
	}
	
	private static HashMap<String,Integer> getCluster(LinkedHashMap<String, HashMap<String, Double>> mainMap){		
		
		LinkedHashMap<String, Double> comparisonMap = getComparisonMap(mainMap);		
		TreeMap<String,Double> sortedComparisonMap = sortMap(comparisonMap);        
        
		LinkedHashSet<String> sortedComparisonSet = new LinkedHashSet<String>();		
		sortedComparisonSet.addAll(sortedComparisonMap.keySet());
		HashMap<String,Integer>clusters=new HashMap<String,Integer>();
		LinkedHashSet<String>alinan=new LinkedHashSet<String>();
		 
		int grup_Id=0;
		int uzunluk=sortedComparisonSet.size();
		int uzun=0;
		System.out.println(uzunluk);
		while(uzun<uzunluk-1){
			
			boolean atla=true;
		grup_Id++;
		for(String str : sortedComparisonSet){		
			uzun++;
			String doc1 = str.substring(0,str.indexOf('<'));
			String doc2 = str.substring(str.indexOf('>')+1);
			double sonuc=cosineSimilarity2(mainMap,doc1,doc2);
			if(sonuc>0.1){
			if(alinan.size()==0){
				clusters.put(doc1,grup_Id);
				clusters.put(doc2,grup_Id);
				
				alinan.add(doc1);
				alinan.add(doc2);
			}
					
			
			else if(alinan.contains(doc1)&&!alinan.contains(doc2)){
				System.out.println("denemedladsa");
				int grup=clusters.get(doc1);
				clusters.put(doc2, grup);
			
				alinan.add(doc2);
				atla=false;
				
			}

			 else if(alinan.contains(doc2)&&!alinan.contains(doc1)){
				 System.out.println("gelesesme�lkrw�kre");
				int grup=clusters.get(doc2);
				clusters.put(doc1, grup);
				alinan.add(doc1);
				
				atla=false;
			}
			 else if(!alinan.contains(doc1)&&!alinan.contains(doc2)&&atla==true){
				 
				 grup_Id++;
				 clusters.put(doc1, grup_Id);
				 clusters.put(doc2, grup_Id);
				 alinan.add(doc1);
				 alinan.add(doc2);
			 }
			 else if(alinan.contains(doc1)&&alinan.contains(doc2)){
				continue; 
			 }
			}
		}
		
		}
			
		
		return clusters;
	}
	
	private static LinkedHashMap<String, LinkedHashSet<String>> getClusters(LinkedHashMap<String, HashMap<String, Double>> mainMap){
		
		int clusterIndex = 0;
		String clusterName = "Cluster_";
		
		LinkedHashMap<String, LinkedHashSet<String>> clusters = new LinkedHashMap<String, LinkedHashSet<String>>();
		
			
			HashMap<String,Integer> cluster = getCluster(mainMap);			

		return clusters;
	}
	
	private static String getClusterName(LinkedHashMap<String, LinkedHashSet<String>> clusters,String doc1Name,String doc2Name){
		boolean c1;
		boolean c2;
		for (Map.Entry<String, LinkedHashSet<String>> entry1 : clusters.entrySet()){
			c1=false;
			c2=false;
			
			String currentClusterName = entry1.getKey();
			LinkedHashSet<String> clusterElements = entry1.getValue();
			
			for(String str : clusterElements){
				
				if(doc1Name.equals(str))
					c1=true;					
				
				if(doc2Name.equals(str))
					c2=true;
				
				if(c1&&c2)
					return currentClusterName;
			}		
		}		
		return "";	
	}
	
//Write TXT
	private static void writeWords(LinkedHashMap<String, HashMap<String, Double>> mainMap) throws FileNotFoundException, UnsupportedEncodingException{
		PrintWriter writer = new PrintWriter(pathRNewsInfo+"words.txt", "UTF-8");						
		String gap1;
		String gap2;
		
		for (Map.Entry<String, HashMap<String, Double>> entry1 : mainMap.entrySet()){						
			writer.print("------------------------------------------------------------------------------------------------\n");
			
			String documentName = entry1.getKey();			
			HashMap<String, Double> currentTFIDFMap = entry1.getValue();
			
			for (Map.Entry<String, Double> entry2 : currentTFIDFMap.entrySet()){
				gap1 = "";
				gap2 = "";
				
				String word = entry2.getKey();
				double tfidf = entry2.getValue();
				
				for(int i=0; i<15-documentName.length(); i++)
					gap1 +=" ";
				for(int i=0; i<15-word.length(); i++)
					gap2 +=" ";
				
				writer.println("Document name: " + documentName + gap1 + "Word: " + word + gap2 + "TFIDF: " + tfidf);				
			}
		}				
		writer.close();		
		System.out.println("->'words.txt' is ready!");
	}

	
	private static void writeWords_Individualy(HashMap<String, HashMap<String, Double>> mainMap) throws FileNotFoundException, UnsupportedEncodingException{
		String gap1;
		String gap2;
		
		for (Map.Entry<String, HashMap<String, Double>> entry1 : mainMap.entrySet()){						
			
			String documentName = entry1.getKey();			
			HashMap<String, Double> currentTFIDFMap = entry1.getValue();			
			
			TreeMap<String, Double> sortedMap = sortMap(currentTFIDFMap);			
			
			PrintWriter writer = new PrintWriter(pathRNewsTFIDF+documentName+"-tfidf.txt", "UTF-8");
			
			for (Map.Entry<String, Double> entry2 : sortedMap.entrySet()){				
				String word = entry2.getKey();
				double tfidf = entry2.getValue();
				
				gap1 = "";
				gap2 = "";
				
				for(int i=0; i<15-documentName.length(); i++)
					gap1 +=" ";
				for(int i=0; i<15-word.length(); i++)
					gap2 +=" ";
				
				writer.println(documentName + gap1 + word + gap2 + tfidf);												
			}			
			writer.close();
		}		
		System.out.println("->'(*)-tfidf.txt' document(s) are ready!");
	}

	private static void writeWordsSorted(LinkedHashMap<String, HashMap<String, Double>> mainMap) throws FileNotFoundException, UnsupportedEncodingException{
		PrintWriter writer = new PrintWriter(pathRNewsInfo+"words-sorted.txt", "UTF-8");		
		String gap1;
		String gap2;
		
		for (Map.Entry<String, HashMap<String, Double>> entry1 : mainMap.entrySet()){						
			writer.print("------------------------------------------------------------------------------------------------\n");
			
			String documentName = entry1.getKey();			
			HashMap<String, Double> currentTFIDFMap = entry1.getValue();
			
			TreeMap<String, Double> sortedMap = sortMap(currentTFIDFMap);
			
			for (Map.Entry<String, Double> entry2 : sortedMap.entrySet()){ 
				gap1 = "";
				gap2 = "";
				
				String word = entry2.getKey();
				double tfdif = entry2.getValue();
				
				for(int i=0; i<15-documentName.length(); i++)
					gap1 +=" ";
				for(int i=0; i<15-word.length(); i++)
					gap2 +=" ";
				
				writer.println("Document name: " + documentName + gap1 + "Word: " + word + gap2 + "TFIDF: " + tfdif);				
			}
		}						
		writer.close();		
		System.out.println("->'words-sorted.txt' is ready!");
	}
	
	private static void writeComparisons(LinkedHashMap<String, HashMap<String, Double>> mainMap) throws FileNotFoundException, UnsupportedEncodingException{
		PrintWriter writer = new PrintWriter(pathRNewsInfo+"comparisons.txt", "UTF-8");						
		
		String gap;
		LinkedHashMap<String, Double> comparisonMap = getComparisonMap(mainMap);
				
		for (Map.Entry<String, Double> entry : comparisonMap.entrySet()){
			
			String str = entry.getKey();
			double similarity = entry.getValue();
			
			String doc1Name = str.substring(0,str.indexOf('<'));
			String doc2Name = str.substring(str.indexOf('>')+1);
			
			String commonWords = getCommonWords(mainMap.get(doc1Name), mainMap.get(doc2Name));
			
			gap = "";
			for(int i=0; i<40-(doc1Name+doc2Name+similarity).length(); i++)
				gap += " ";
			System.out.println(doc1Name+"   "+doc2Name);
			writer.println("Similarity of ("+doc1Name+") and ("+doc2Name+") is ("+similarity+") "+ gap +"Common words are: "+commonWords);
					
			}
		writer.close();
		System.out.println("->'comparisons.txt' is ready!");
	}
	public static ResultSet Insertcossim(LinkedHashMap<String,Double>map){
		ResultSet sonuc=null;
		try{
			Veritabani.baglan();
			for (Map.Entry<String, Double> entry : map.entrySet()){
				
				String str = entry.getKey();
				double similarity = entry.getValue();
				
				String doc1 = str.substring(0,str.indexOf('<'));
				String doc2 = str.substring(str.indexOf('>')+1);
			if(similarity>0.3){
				PreparedStatement sorgu=Veritabani.getBaglanti().prepareStatement("Insert into cossim values ('"+doc1+"','"+doc2+"','"+similarity+"');");
			sorgu.executeUpdate();
			}
			}
		}
	   catch (Exception e) {
          System.out.println(e.getMessage());
      }
		return sonuc;
	}
	public static ResultSet Inserttopic(ArrayList<String>list1,ArrayList<String>list2){
		ResultSet sonuc=null;
		try{
			Veritabani.baglan();
			for(int i=0;i<list1.size();i++){
				PreparedStatement sorgu=Veritabani.getBaglanti().prepareStatement("Insert into newstopics values('"+list1.get(i)+"','"+list2.get(i)+"');");
				sorgu.executeUpdate();
				System.out.print(list1.get(i));
			}
		}
		  catch (Exception e) {
	          System.out.println(e.getMessage());
	      }
		return sonuc;
	}
	public static ResultSet getSayi( HashMap<String,Integer> kontrol,String []array){
	ResultSet sonuc=null;
	Veritabani.baglan();
		try{
			
			 for(int t=0;t<array.length;t++){
				 String doc=array[t];
				 PreparedStatement sorgu=Veritabani.getBaglanti().prepareStatement("select DISTINCT topicname from newstopics where docname='"+doc+"';");
				 sonuc=sorgu.executeQuery();
			  System.out.println(array[t]);
			 int y=0;
			 while(sonuc.next()){
				 y++;
			 }
			 if(y>1){
				 kontrol.put(array[t], y);
			 }
			 }
		}
		catch (Exception e) {
	        System.out.println(e.getMessage());
	    }
		return sonuc;
	}
public static void newsgrup( ArrayList<ArrayList<String>> grup){
	try{
		Veritabani.baglan();
		for(int i=0;i<grup.size();i++){
			if(grup.get(i).size()>2){
			for(int k=0;k<grup.get(i).size();k++){
				PreparedStatement sorgu=Veritabani.getBaglanti().prepareStatement("Insert into newstopicscluster values('"+(i+1)+"','"+grup.get(i).get(k)+"');");
				sorgu.executeUpdate();
			}
			}
		}
	
	
	
	}
	catch (Exception e) {
        System.out.println(e.getMessage());
    }
	}
	
	private static void writeSortedComparisons(LinkedHashMap<String, HashMap<String, Double>> mainMap) throws FileNotFoundException, UnsupportedEncodingException{
		PrintWriter writer = new PrintWriter(pathRNewsInfo+"comparisons-sorted.txt", "UTF-8");							
		
		String gap;		
		LinkedHashMap<String, Double> comparisonMap = getComparisonMap(mainMap);		
        TreeMap<String,Double> sortedMap = sortMap(comparisonMap);        
//        ResultSet sonuc=null;
//    	sonuc=Insertcossim(comparisonMap);
    	for (Map.Entry<String, Double> entry : sortedMap.entrySet()){
			String str = entry.getKey();
			double similarity = entry.getValue();
			
			String doc1Name = str.substring(0,str.indexOf('<'));
			String doc2Name = str.substring(str.indexOf('>')+1);
    		
			String commonWords = getCommonWords(mainMap.get(doc1Name), mainMap.get(doc2Name));
			gap = "";
			for(int i=0; i<40-(doc1Name+doc2Name+similarity).length(); i++)
				gap += " ";
			
			writer.println("Similarity of ("+doc1Name+") and ("+doc2Name+") is ("+similarity+") "+ gap +"Common words are: "+commonWords);
        }        

		writer.close();
		System.out.println("->'comparisons-sorted.txt' is ready!");
	}
	
	private static void writeClusters(LinkedHashMap<String, HashMap<String, Double>> mainMap) throws FileNotFoundException, UnsupportedEncodingException{
		PrintWriter writer = new PrintWriter(pathRNewsInfo+"clusters.txt", "UTF-8");				
		
		String gap;
		HashMap<String,Integer> cluster=new HashMap<String,Integer>();
		cluster=getCluster2(mainMap);
		int maxgrup=0;
		for(Map.Entry<String,Integer>entry:cluster.entrySet()){
			String documentName=entry.getKey();
			int deger=entry.getValue();
			if(maxgrup<deger){
				maxgrup=deger;
			}
			
			
		}
		
//		Map.Entry<String, HashMap<String, Double>> entry1 : mainMap.entrySet()
		
		for(int k=1;k<=maxgrup;k++){
			writer.print("Cluster "+k+" :[" );
			ArrayList<String>liste=new ArrayList<String>();
			
		for(Map.Entry<String,Integer>entry:cluster.entrySet()){
			
				String documentName=entry.getKey();
				if(entry.getValue().equals(k)){
					liste.add(documentName);
					writer.print(documentName+" ");
				}
			}
		grupcluster.add(liste);
		writer.print("]");
		writer.println();
		
			
		}
		
		
//		LinkedHashMap<String, LinkedHashSet<String>> clusters = getClusters(new LinkedHashMap<String, HashMap<String, Double>>(mainMap));		
//		
//		for (Map.Entry<String, LinkedHashSet<String>> entry1 : clusters.entrySet()){			
//			gap = "";
//			
//			String currentClusterName = entry1.getKey();
//			LinkedHashSet<String> currentCluster = entry1.getValue();
//			
//			for(int i=0; i<14-currentClusterName.length(); i++)
//				gap += " ";
//			
//																	
//		}		
		writer.close();
		System.out.println("->'clusters.txt' is ready!");
		insertcluster(cluster);
	}
	public static void Writetopiccluster(ArrayList<ArrayList<String>> grup) throws FileNotFoundException, UnsupportedEncodingException{
		PrintWriter writer = new PrintWriter(pathRNewsInfo+"topicclusters.txt", "UTF-8");
		PrintWriter writer2 = new PrintWriter(pathRNewsInfo+"deneme.txt", "UTF-8");
		LinkedHashSet<String>alinan=new LinkedHashSet<String>();
		int sayi=0;
		for(int i=0;i<grup.size();i++){
			ArrayList<String>liste=new ArrayList<String>();
			if(grup.get(i).size()>2){
				
			
				for(int k=0;k<grup.get(i).size();k++){
				if(!alinan.contains(grup.get(i).get(k))){
					alinan.add(grup.get(i).get(k));
//					writer.print(grup.get(i).get(k)+"  ");
					}
					liste.add(grup.get(i).get(k));
					
				}
				sayi++;
				writer.println("Cluster "+sayi+"---"+liste+"---");
				writer2.println();
				
//				writer.println(grup.get(i));
			}
			if(liste.size()>1){
//				gruptopic.add(grup.get(i));
				gruptopic.add(liste);
				writer2.print(liste);
				}
			
		}
	
		writer.close();
		writer2.close();
		
	}
	public static void ReadAlchemy() throws FileNotFoundException, UnsupportedEncodingException{
		String []files=getXMLNames();
		File file = null;
	    FileReader fr = null;
	    PrintWriter writer = new PrintWriter(pathR+"deneme.txt", "UTF-8");
	    try {     
           
            File fXmlFile = new File(pathR+"3.xml");  

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();  
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();      
            Document doc = dBuilder.parse(fXmlFile);      
            doc.getDocumentElement().normalize();    
            NodeList nList = doc.getElementsByTagName("dbpedia");     
            for (int i = 0; i < nList.getLength(); i++) 
            {      
                Node node = nList.item(i);    
                if (node.getNodeType() == Node.ELEMENT_NODE)    
                {         
                     Element eElement = (Element) node; 
                    if(eElement.hasChildNodes())    
                    {               
                        NodeList nl = node.getChildNodes();      
                        for(int j=0; j<nl.getLength(); j++)  
                        {                   
                            Node nd = nl.item(j);   
                            String name= nd.getTextContent();
                            
                              if (name != null && !name.trim().equals(""))                                 {
//                                   System.out.print(name.trim()+",");   
                                   //System.out.print(" ");
                                   boolean varmi=false;
                                   String deger=nd.getTextContent().trim();
                                   System.out.println(deger);
                                		   
                                
                                
                          		
                                  
                               

                        } 
                              writer.println(nd.getTextContent().toString()); 
                        System.out.println("");
                        writer.write("\n");
                        }        
                        } }  
            writer.close();
            } 
	    }
        catch (Exception e) {       
                        e.printStackTrace();     } } 
	    
	    
//	    LineNumberReader lnr = null;
//	    ArrayList<String> myList = new ArrayList<String>();
//	    ArrayList<String>myList2=new ArrayList<String>();
//	    ArrayList<String>myList3=new ArrayList<String>();
//	    HashMap<Integer,String>map=new HashMap<Integer,String>();
//	  for(int i=0;i<files.length;i++){
//	    try {
//	        file = new File(pathR);
//	        fr = new FileReader(file);           
//	        lnr = new LineNumberReader(fr);
//	        String line = "";           
//	        boolean denetleme=false;
//	        while ((line = lnr.readLine()) != null&& denetleme!=true) {
//	        	boolean varmi=false;
//        		for(int t=0;t<line.length();t++){
//        			for(int j=i+1;j<=line.length();j++){
//        				if(line.substring(t,j).equalsIgnoreCase("<dbpedia>")){
//        					varmi=true;
//        					System.out.println("deneem");
//        				}
//        				}
//        			
//        		}
//	        }
//	  
//	    
//	    }catch(Exception e){
//	    	System.out.println(e.getMessage());
//	    }
//	    
	        
	
	
	
	
	public static ResultSet insertcluster(HashMap<String,Integer> cluster){
		ResultSet sonuc=null;
		Veritabani.baglan();
		int maxgrup=0;
		for(Map.Entry<String,Integer>entry:cluster.entrySet()){
			String documentName=entry.getKey();
			int deger=entry.getValue();
			if(maxgrup<deger){
				maxgrup=deger;
			}
			
			
		}
		try{
			for(int k=1;k<=maxgrup;k++){
			
				
			for(Map.Entry<String,Integer>entry:cluster.entrySet()){
				
					String documentName=entry.getKey();
					if(entry.getValue().equals(k)){
						System.out.println(documentName);
						PreparedStatement sorgu=Veritabani.getBaglanti().prepareStatement("Insert into cluster values('"+k+"','"+documentName+"');");
						sorgu.executeUpdate();
					}
				}
		
			
				
			}
			
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return sonuc;
	}
	public static ResultSet getGrup(String topic){
		ResultSet sonuc=null;
		Veritabani.baglan();
		try{
			PreparedStatement sorgu=Veritabani.getBaglanti().prepareStatement("select docname from newstopics where topicname='"+topic+"';");
			sonuc=sorgu.executeQuery();
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		
		return sonuc;
		
	}
	public static ResultSet getDoc1(String doc) {
		ResultSet sonuc=null;
		Veritabani.baglan();
		try{
			PreparedStatement sorgu=Veritabani.getBaglanti().prepareStatement("Select doc2 from cossim where doc1='"+doc+"'and sim>0.45");
			sonuc=sorgu.executeQuery();
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return sonuc;
	}
	public static ResultSet Topic(){
		ResultSet sonuc=null;
		Veritabani.baglan();
		try{
			PreparedStatement sorgu=Veritabani.getBaglanti().prepareStatement("select distinct topicname from newstopics;");
			sonuc=sorgu.executeQuery();
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		
		return sonuc;
	}
	private static void writeResults(LinkedHashMap<String, HashMap<String, Double>> mainMap) throws FileNotFoundException, UnsupportedEncodingException{
		PrintWriter writer = new PrintWriter(pathRNewsInfo+"results.txt", "UTF-8");							
				
		String gap;		
		LinkedHashMap<String, Double> comparisonMap = getComparisonMap(mainMap);		
		LinkedHashMap<String, LinkedHashSet<String>> clusters = getClusters(new LinkedHashMap<String, HashMap<String,Double>>(mainMap));        
                
    	for (Map.Entry<String, Double> entry : comparisonMap.entrySet()){
			String str = entry.getKey();
			double similarity = entry.getValue();
			
			String doc1Name = str.substring(0,str.indexOf('<'));
			String doc2Name = str.substring(str.indexOf('>')+1);
    		
			String commonWords = getCommonWords(mainMap.get(doc1Name), mainMap.get(doc2Name));
			
			String clusterName = getClusterName(clusters, doc1Name, doc2Name);
			
			gap = "";
			for(int i=0; i<50-(doc1Name+doc2Name+similarity+clusterName).length(); i++)
				gap += " ";
			
			if(!clusterName.equals(""))
				writer.println(clusterName+": ("+doc1Name+","+doc2Name+") Similarity: "+similarity+gap+"Common words: "+commonWords);
        }        

		writer.close();
		System.out.println("->'results.txt' is ready!");
	}
	
	private static void writeSortedResults(LinkedHashMap<String, HashMap<String, Double>> mainMap) throws FileNotFoundException, UnsupportedEncodingException{
		PrintWriter writer = new PrintWriter(pathRNewsInfo+"results-sorted.txt", "UTF-8");							
				
		String gap;		
		LinkedHashMap<String, Double> comparisonMap = getComparisonMap(mainMap);		
		LinkedHashMap<String, LinkedHashSet<String>> clusters = getClusters(new LinkedHashMap<String, HashMap<String,Double>>(mainMap));
        TreeMap<String,Double> sortedMap = sortMap(comparisonMap);        
                
    	for (Map.Entry<String, Double> entry : sortedMap.entrySet()){
			String str = entry.getKey();
			double similarity = entry.getValue();
			
			String doc1Name = str.substring(0,str.indexOf('<'));
			String doc2Name = str.substring(str.indexOf('>')+1);
    		
			String commonWords = getCommonWords(mainMap.get(doc1Name), mainMap.get(doc2Name));
			
			String clusterName = getClusterName(clusters, doc1Name, doc2Name);
			
			gap = "";
			for(int i=0; i<50-(doc1Name+doc2Name+similarity+clusterName).length(); i++)
				gap += " ";
			
			if(!clusterName.equals(""))
				writer.println(clusterName+": ("+doc1Name+","+doc2Name+") Similarity: "+similarity+gap+"Common words: "+commonWords);
        }        
		writer.close();
		System.out.println("->'results-sorted.txt' is ready!");
	}
	
	public static void run() throws IOException, SQLException{
		System.out.println("Program started.");		

		LinkedHashMap<String, HashMap<String, Double>> mainMap = getAll(); 
		
		if(mainMap != null && mainMap.size() != 0){
			int numberOfDocuments = mainMap.size();

			System.out.println("\nPlease choose one of the following options.");
			System.out.println("1: write words.");
			System.out.println("2: write words individualy.");
			System.out.println("3: write words sorted.");
			System.out.println("4: write comparisons.");
			System.out.println("5: write comparisons sorted.");
			System.out.println("6: write clusters.");
			System.out.println("7: write results.");
			System.out.println("8: write results sorted.");
			System.out.println("0: write all.");

			Scanner keyb  = new Scanner(System.in);
			int sck = keyb.nextInt();
			
			long start = System.currentTimeMillis();
			switch (sck) {
			case 1:
				System.out.println("\nWriting words...");
				writeWords(mainMap);
				
				break;
			case 2:
				System.out.println("\nWriting words individualy...");
				writeWords_Individualy(mainMap);
				break;
			case 3:
				System.out.println("\nWriting words sorted...");
				writeWordsSorted(mainMap);		
				break;
			case 4:
				System.out.println("\nWriting comparisons...");
				writeComparisons(mainMap);
				break;
			case 5:
				System.out.println("\nWriting comparisons sorted...");
				writeSortedComparisons(mainMap);
				break;
			case 6:
				System.out.println("\nWriting clusters...");
				writeClusters(mainMap);
				break;	
			case 7:
				System.out.println("\nWriting results...");			
				writeResults(mainMap);
				break;	
			case 8:
				System.out.println("\nWriting results sorted...");			
				writeSortedResults(mainMap);
				break;		
			case 0:
				System.out.println("\nWriting all...");
//				writeWords(mainMap);
//				writeWords_Individualy(mainMap);		
//				writeWordsSorted(mainMap);		
//				writeComparisons(mainMap);		
//				System.out.println("1234");
//				writeSortedComparisons(mainMap);
//				System.out.println("Sorted");
				writeClusters(mainMap);
				System.out.println("Cluster");
//				writeResults(mainMap);
//				writeSortedResults(mainMap);
//				
				break;
				
			default:
				System.out.println("\nWriting all...");
				writeWords(mainMap);
				writeWords_Individualy(mainMap);		
				writeWordsSorted(mainMap);		
				writeComparisons(mainMap);		
				writeSortedComparisons(mainMap);		
				writeClusters(mainMap);
				writeResults(mainMap);
				writeSortedResults(mainMap);
			
			}		
			long end = System.currentTimeMillis();
			
			double elapsedTime = (double)(end-start)/1000;
			System.out.println("Documents are processed and requested file(s) created.");						
			System.out.println("\nElapsed time: "+elapsedTime+" seconds.");
			System.out.println("Processing time for each document: "+elapsedTime/numberOfDocuments+" seconds.");
			System.out.println("Processing time for 20.000 documents: "+((elapsedTime/numberOfDocuments)*20000)/60+" minutes.");
		}		
		System.out.println("\nProgram terminated.");
	}
}