package src;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class Veritabani{

    public static Connection baglanti;


    public static Connection getBaglanti() {
        return baglanti;
    }

    public static void baglan() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            baglanti = DriverManager.getConnection("jdbc:mysql://localhost:3306/bitirme", "root", "");

            System.out.println("connected");
           


        } catch (Exception e) {
            System.out.println("connection problem");
        }

    }
    
    public static void executeQuery(String query) throws SQLException {
        System.out.println(query);
        baglanti.createStatement().executeUpdate(query);

    }

    public static ResultSet getResultSet(String query) throws SQLException {
        return baglanti.createStatement().executeQuery(query);
    }
    
      
}